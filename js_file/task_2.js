document.getElementById("btn-do-task-2").addEventListener("click", function () {
  // Tạo biển lấy cơ số (x)
  var baseNumberValue = document.getElementById("task-2-base-number").value * 1;
  // Tạo biến lấy giá trị số mũ người dùng nhập (n)
  var exponentValue = document.getElementById("task-2-exponent").value * 1;
  // Biến kiểm tra sự tăng giảm của số mũ sau mỗi vòng lặp
  var exponentValueString = "";
  // Tạo biến lấy phần nguyên của số mũ (vd: 3.5 -> 3)
  var exponentIntValue = parseInt(exponentValue);
  // Biến kiểm tra sự tăng giảm của số mũ sau mỗi vòng lặp đối với mũ chỉ lấy phần số nguyên
  var exponentIntValueString = "";
  // Tổng
  var sum = 0;
  // Tổng với giá trị số mũ chỉ lấy phần số nguyên
  var sumWithExponentIntValue = 0;
  // Thẻ in kết quả
  var resultDiv = document.getElementById("task-2-result-div");

  // Kiểm tra điều kiện nếu số mũ lớn hơn 0!
  if (exponentValue > 0) {
    // Vòng lặp cho số mũ lấy cả phần thập phân
    for (exponentValue; exponentValue > 0; exponentValue--) {
      // Tổng = Tổng + math.pow(cơ số, số mũ)
      sum += Math.pow(baseNumberValue, exponentValue);
      exponentValueString += exponentValue + "  ";
    }
    // Vòng lặp cho số mũ chỉ lấy phần số nguyên
    for (i = 1; i <= exponentIntValue; i++) {
      sumWithExponentIntValue += Math.pow(baseNumberValue, i);
      exponentIntValueString += i + "  ";
    }
  } else {
    // Nếu  trường hợp số mũ bằng 0 thì trả về kết quả Tổng = 1!
    if (exponentValue == 0) {
      sum = 1;
      sumWithExponentIntValue = 1;
    } else {
      // Nếu số mũ âm thì cộng dần đến khi số mũ lớn hơn 0 thì dừng
      // -3.5 => -2.5 => -1.5 => -0.5
      for (exponentValue; exponentValue < 0; exponentValue++) {
        sum += Math.pow(baseNumberValue, exponentValue);
        exponentValueString += exponentValue + "  ";
      }
      for (i = -1; i >= exponentIntValue; i--) {
        sumWithExponentIntValue += Math.pow(baseNumberValue, i);
        exponentIntValueString += i + "  ";
      }
    }
  }
  // In ra kết quả
  resultDiv.innerHTML = `<div class="mt-4 px-4">
    <label for="">Tính tổng với số mũ lấy cả phần thập phân:</label>
    <p class="mb-0 text-primary text-center">${sum}</p>
    <label for="">Tính tổng với số mũ chỉ lấy phần số nguyên:</label>
    <p class="mb-0 text-primary text-center">${sumWithExponentIntValue}</p>
  </div>`;
  // Console kiểm tra lỗi
  console.log("số mũ lần lượt: ", exponentValueString);
  console.log({ sum });
  console.log("số mũ lần lượt: ", exponentIntValueString);
  console.log({ sumWithExponentIntValue });
  console.log({ exponentValue, exponentIntValue });
  console.log({ baseNumberValue });
});
