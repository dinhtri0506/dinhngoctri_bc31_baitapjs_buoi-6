document.getElementById("btn-do-task-4").addEventListener("click", function () {
  var result = "";
  var resultDiv = document.getElementById("task-4-result-div");
  for (i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      result += `<div class="col-8 px-4 py-3 bg-danger text-white">${i} div chẵn</div>`;
    } else {
      result += `<div class="col-8 px-4 py-3 bg-primary text-white">${i} div lẻ</div>`;
    }
  }
  resultDiv.innerHTML = result;
});
