document.getElementById("btn-do-task-1").addEventListener("click", function () {
  // Tạo biến cộng tổng
  var sum = 0;
  // Tạo vòng lặp chạy vô tận, chỉ dừng lại khi
  for (i = 1; i > 0; i++) {
    // Tổng lớn hơn 10.000
    if (sum > 10000) {
      // Dừng vòng lặp
      break;
    }
    // Nếu tổng chưa lớn hơn 10.000, tiếp tục lấy tổng cộng i
    else {
      sum += i;
      console.log(i);
    }
  }
  // In kết quả
  document.getElementById(
    "task-1-result-div"
  ).innerHTML = `<div class="mt-4 px-4">
  <label for="">Số nguyên dương nhỏ nhất:</label>
  <p class="display-4 mb-0 text-primary text-center">${i - 1}</p>
</div>`;
  console.log(sum);
});
