document.getElementById("btn-do-task-3").addEventListener("click", function () {
  // Tạo biến lấy giá trị người dùng nhập
  var numberValue = document.getElementById("task-3-input").value * 1;
  // Kết quả giai thừa
  var numberFactorial = 1;
  // Nếu giá trị nhập vào nhỏ hơn hoặc bằng 0 thì kết quả giai thừa bằng 1
  if (numberValue <= 0) {
    numberFactorial = 1;
  }
  // Ngược lại tạo vòng lặp
  else {
    for (i = 1; i <= numberValue; i++) {
      numberFactorial *= i;
    }
  }
  // In kết quả giai thừa
  document.getElementById(
    "task-3-result-div"
  ).innerHTML = `<div class="mt-4 px-4">
  <label for="">Tính gia thừa với n! = ${numberValue}!</label>
  <p class="mb-0 text-primary text-center">${new Intl.NumberFormat(
    "de-DE",
    {}
  ).format(numberFactorial)}</p>
 </div>`;
});
